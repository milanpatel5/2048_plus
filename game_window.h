#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QAction>
#include <QEvent>
#include <QThread>
#include <assert.h>
#include <iomanip>
#include <QTableWidget>
#include <QString>

namespace Ui 
{
    class Game_Window;
}

class Game_Window : public QMainWindow
{
    Q_OBJECT
    
    public:
        explicit Game_Window(QWidget *parent = 0);
        ~Game_Window();
        
    protected:
        void changeEvent(QEvent *e);
        
    private slots:
        void UpdateBoardLeft();
        void UpdateBoardRight();
        void UpdateBoardUp();
        void UpdateBoardDown();
        void UpdateBoardLeft(int **);
        void UpdateBoardRight(int **);
        void UpdateBoardUp(int **);
        void UpdateBoardDown(int **);
        void InitialBoard();
        void DisplayBoard();
        void AddTile(int **, int );
        double CalcHeuristic(int **, int, int, int);
        double OpenSquares(int **);
        double MergeCount(int **, int **);
        double BigTilesAtBorder(int **);
        double AdjacentPairDifference(int **);
        void GameOver();
        
    private:
        Ui::Game_Window *ui;
        QAction *Up_key;
        QAction *Down_key;
        QAction *Left_key;
        QAction *Right_key;
        int **Board;
        const double OPEN_SQUARES = 20.50;
        const double MERGE_COUNT = 3.50;
        const double BIG_TILE_AT_BORDER = 5.50;
        const double SMOOTHNESS = 5.50;
        int Depth = 2;
        const bool Debug = false;       //Function Calls
        const bool Debug1 = false;      //Heuristics
};

class Thread : public QThread
{
    Q_OBJECT
    
    public:
        Thread();
    
    signals:
    
    public slots:
        void run();
    
};

#endif // GAME_WINDOW_H
