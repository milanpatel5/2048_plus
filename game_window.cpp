#include "game_window.h"
#include "ui_game_window.h"

Game_Window::Game_Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Game_Window)
{
    //  Random Seed
    srand(time(NULL));
    
    //  Key Bindings
    Up_key = new QAction(this);
    Up_key->setShortcut(Qt::Key_Up);
    addAction(Up_key);
    connect(Up_key,SIGNAL(triggered()),this,SLOT(UpdateBoardUp()));
 
    Down_key = new QAction(this);
    Down_key->setShortcut(Qt::Key_Down);
    addAction(Down_key);
    connect(Down_key,SIGNAL(triggered()),this,SLOT(UpdateBoardDown()));
    
    Left_key = new QAction(this);
    Left_key->setShortcut(Qt::Key_Left);
    addAction(Left_key);
    connect(Left_key,SIGNAL(triggered()),this,SLOT(UpdateBoardLeft()));
    
    Right_key = new QAction(this);
    Right_key->setShortcut(Qt::Key_Right);
    addAction(Right_key);
    connect(Right_key,SIGNAL(triggered()),this,SLOT(UpdateBoardRight()));
    
    //  Memory Allocation
    Board = new int*[4];
    for(int i=0;i<4;++i)
        Board[i] = new int[4];
    
    //  Initialization
    this->setWindowFlags(Qt::FramelessWindowHint);
    ui->setupUi(this);
    ui->textBrowser->hide();
    ui->GameBoard->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->GameBoard->setSelectionMode(QTableWidget::NoSelection);
    for(int i=0;i<4;++i)
    {
        for(int j=0;j<4;++j)
        {
            ui->GameBoard->setItem(i,j,new QTableWidgetItem(Board[i][j]));
            ui->GameBoard->item(i,j)->setTextAlignment(Qt::AlignCenter);
            ui->GameBoard->item(i,j)->setBackgroundColor(QColor(0,0,204));
            ui->GameBoard->item(i,j)->setForeground(QColor(255,255,255));
        }
    }
    InitialBoard();
}

double Game_Window::CalcHeuristic(int **Board, int depth, int x, int y)
{
    int **LBoard = new int*[4];
    for(int i=0;i<4;++i)
    {
        LBoard[i] = new int[4];
        for(int j = 0;j<4;++j)
            LBoard[i][j] = Board[i][j];
    }
    LBoard[x][y] = 1;
    UpdateBoardLeft(LBoard);
    if(depth)
        AddTile(LBoard,depth-1);
    double LHeuristic = (OPEN_SQUARES*OpenSquares(LBoard))
                        + (MERGE_COUNT*MergeCount(Board,LBoard))
                        - (BIG_TILE_AT_BORDER*BigTilesAtBorder(LBoard))
                        - (SMOOTHNESS * AdjacentPairDifference(LBoard));
    for(int i=0;i<4;++i)
        free(LBoard[i]);
    free(LBoard);

    int **RBoard = new int*[4];
    for(int i=0;i<4;++i)
    {
        RBoard[i] = new int[4];
        for(int j = 0;j<4;++j)
            RBoard[i][j] = Board[i][j];
    }
    RBoard[x][y] = 1;
    UpdateBoardRight(RBoard);
    if(depth)
        AddTile(RBoard,depth-1);
    double RHeuristic = (OPEN_SQUARES*OpenSquares(RBoard))
                        + (MERGE_COUNT*MergeCount(Board,RBoard))
                        - (BIG_TILE_AT_BORDER*BigTilesAtBorder(RBoard));
                        - (SMOOTHNESS * AdjacentPairDifference(RBoard));
    for(int i=0;i<4;++i)
        free(RBoard[i]);
    free(RBoard);

    int **UBoard = new int*[4];
    for(int i=0;i<4;++i)
    {
        UBoard[i] = new int[4];
        for(int j = 0;j<4;++j)
            UBoard[i][j] = Board[i][j];
    }
    UBoard[x][y] = 1;
    UpdateBoardUp(UBoard);
    if(depth)
        AddTile(UBoard,depth-1);
    double UHeuristic = (OPEN_SQUARES*OpenSquares(UBoard))
                        + (MERGE_COUNT*MergeCount(Board,UBoard))
                        - (BIG_TILE_AT_BORDER*BigTilesAtBorder(UBoard));
                        - (SMOOTHNESS * AdjacentPairDifference(UBoard));
    for(int i=0;i<4;++i)
        free(UBoard[i]);
    free(UBoard);

    int **DBoard = new int*[4];
    for(int i=0;i<4;++i)
    {
        DBoard[i] = new int[4];
        for(int j = 0;j<4;++j)
            DBoard[i][j] = Board[i][j];
    }
    DBoard[x][y] = 1;
    UpdateBoardDown(DBoard);
    if(depth)
        AddTile(DBoard,depth-1);
    double DHeuristic = (OPEN_SQUARES*OpenSquares(DBoard))
                        + (MERGE_COUNT*MergeCount(Board,DBoard))
                        - (BIG_TILE_AT_BORDER*BigTilesAtBorder(DBoard));
                        - (SMOOTHNESS * AdjacentPairDifference(DBoard));
    for(int i=0;i<4;++i)
        free(DBoard[i]);
    free(DBoard);

    return std::max(std::max(LHeuristic,RHeuristic), std::max(UHeuristic,DHeuristic));
}

void Game_Window::AddTile(int **Board,int depth)
{
    bool Game_Over = true;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            if(Board[i][j] == 0)
                Game_Over = false;
    if(Game_Over && Depth == depth)
        GameOver();
//    assert(!(Game_Over && Depth == depth));
    double **heuristic = new double*[4];
    for(int i=0;i<4;++i)
        heuristic[i] = new double[4];
    for(int i=0;i<4;++i)
    {
        for(int j=0;j<4;++j)
        {
            heuristic[i][j] = 1e6;
            if(Board[i][j] == 0)
                heuristic[i][j] = CalcHeuristic(Board,depth,i,j);
        }
    }
    int min_i = 0, min_j = 0;
    for(int i=0;i<4;++i)
    {
        for(int j=0;j<4;++j)
        {
            if(heuristic[i][j] <= heuristic[min_i][min_j])
            {
                min_i = i;
                min_j = j;
            }
        }
    }
    if(Debug1 && (depth == Depth))
    {
        qDebug() << "Heuristics:";
        for(int i=0;i<4;++i)
            qDebug() << heuristic[i][0] << " " << heuristic[i][1] << " " << heuristic[i][2] << " " << heuristic[i][3] << " ";
    }
    for(int i=0;i<4;++i)
        free(heuristic[i]);
    free(heuristic);
    Board[min_i][min_j] = (((rand()%2) && !(rand()%2)) && (rand()%2))+1;
}

double Game_Window::AdjacentPairDifference(int **Board)
{
    int Diff_sum = 0;
    for(int i = 0;i<4;++i)
    {
        for(int j = 0;j<4;++j)
        {
            if(Board[i][j])
            {
                for(int k = j+1;k<4;++k)
                {
                    if(Board[i][k])
                        Diff_sum += abs(Board[i][j] - Board[i][k]);
                }
                for(int k = i+1;k<4;++k)
                {
                    if(Board[k][j])
                        Diff_sum += abs(Board[i][j] - Board[k][j]);
                }
            }
        }
    }
    return Diff_sum;
}

double Game_Window::OpenSquares(int **Board)
{
    int count = 0;
    for(int i = 0;i<4;i++)
    {
        for(int j = 0;j<4;j++)
        {
            count+=(Board[i][j] == 0);
        }
    }
    return count;
}

double Game_Window::MergeCount(int **before, int **after)
{
    return (OpenSquares(after)-OpenSquares(before));
}

double Game_Window::BigTilesAtBorder(int **Board)
{
    double var = 0;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            if(Board[i][j])
                var += (Board[i][j]*(1 + std::min(std::min(abs(4-i),abs(4-j)), std::min(i,j))));
    return var;
}

void Game_Window::UpdateBoardLeft(int **Board)
{
    if(Debug)
        qDebug() << "In UpdateBoardLeft_H";
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=0;j<4;++j)
        {
            if(Board[i][j])
            {
                int k = j-1;
                while((k >=0)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k+1]);
                    --k;
                }
            }
        }
    }
    for(int i=0;i<4;++i)        //Merge
    {
        for(int j=0;j<3;++j)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i][j+1]))
            {
                Board[i][j]++;
                Board[i][j+1] = 0;
            }
        }
    }
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=0;j<4;++j)
        {
            if(Board[i][j])
            {
                int k = j-1;
                while((k >=0)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k+1]);
                    --k;
                }
            }
        }
    }
}

void Game_Window::UpdateBoardRight(int **Board)
{
    if(Debug)
        qDebug() << "In UpdateBoardRight_H";
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=3;j>=0;--j)
        {
            if(Board[i][j])
            {
                int k = j+1;
                while((k < 4)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k-1]);
                    ++k;
                }
            }
        }
    }
    for(int i=0;i<4;++i)        //Merge
    {
        for(int j=3;j>0;--j)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i][j-1]))
            {
                Board[i][j]++;
                Board[i][j-1] = 0;
            }
        }
    }
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=3;j>=0;--j)
        {
            if(Board[i][j])
            {
                int k = j+1;
                while((k < 4)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k-1]);
                    ++k;
                }
            }
        }
    }
}

void Game_Window::UpdateBoardDown(int **Board)
{
    if(Debug)
        qDebug() << "In UpdateBoardDown_H";
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=3;i>=0;--i)
        {
            if(Board[i][j])
            {
                int k = i+1;
                while((k < 4)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k-1][j]);
                    ++k;
                }
            }
        }
    }
    for(int j=0;j<4;++j)        //Merge
    {
        for(int i=3;i>0;--i)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i-1][j]))
            {
                Board[i][j]++;
                Board[i-1][j] = 0;
            }
        }
    }
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=3;i>=0;--i)
        {
            if(Board[i][j])
            {
                int k = i+1;
                while((k < 4)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k-1][j]);
                    ++k;
                }
            }
        }
    }
}

void Game_Window::UpdateBoardUp(int **Board)
{
    if(Debug)
        qDebug() << "In UpdateBoardUp_H";
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=0;i<4;++i)
        {
            if(Board[i][j])
            {
                int k = i-1;
                while((k >=0)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k+1][j]);
                    --k;
                }
            }
        }
    }
    for(int j=0;j<4;++j)        //Merge
    {
        for(int i=0;i<3;++i)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i+1][j]))
            {
                Board[i][j]++;
                Board[i+1][j] = 0;
            }
        }
    }
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=0;i<4;++i)
        {
            if(Board[i][j])
            {
                int k = i-1;
                while((k >=0)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k+1][j]);
                    --k;
                }
            }
        }
    }
}

void Game_Window::UpdateBoardLeft()
{
    if(Debug)
        qDebug() << "In UpdateBoardLeft";
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=0;j<4;++j)
        {
            if(Board[i][j])
            {
                int k = j-1;
                while((k >=0)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k+1]);
                    --k;
                }
            }
        }
    }
    for(int i=0;i<4;++i)        //Merge
    {
        for(int j=0;j<3;++j)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i][j+1]))
            {
                Board[i][j]++;
                Board[i][j+1] = 0;
            }
        }
    }
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=0;j<4;++j)
        {
            if(Board[i][j])
            {
                int k = j-1;
                while((k >=0)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k+1]);
                    --k;
                }
            }
        }
    }
    DisplayBoard();
    qApp->processEvents();
    AddTile(Board,Depth);
    DisplayBoard();
}

void Game_Window::UpdateBoardRight()
{
    if(Debug)
        qDebug() << "In UpdateBoardRight";
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=3;j>=0;--j)
        {
            if(Board[i][j])
            {
                int k = j+1;
                while((k < 4)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k-1]);
                    ++k;
                }
            }
        }
    }
    for(int i=0;i<4;++i)        //Merge
    {
        for(int j=3;j>0;--j)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i][j-1]))
            {
                Board[i][j]++;
                Board[i][j-1] = 0;
            }
        }
    }
    for(int i=0;i<4;++i)        //Swap
    {
        for(int j=3;j>=0;--j)
        {
            if(Board[i][j])
            {
                int k = j+1;
                while((k < 4)  && (Board[i][k] == 0))
                {
                    std::swap(Board[i][k],Board[i][k-1]);
                    ++k;
                }
            }
        }
    }
    DisplayBoard();
    qApp->processEvents();
    AddTile(Board,Depth);
    DisplayBoard();
}

void Game_Window::UpdateBoardDown()
{
    if(Debug)
        qDebug() << "In UpdateBoardDown";
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=3;i>=0;--i)
        {
            if(Board[i][j])
            {
                int k = i+1;
                while((k < 4)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k-1][j]);
                    ++k;
                }
            }
        }
    }
    for(int j=0;j<4;++j)        //Merge
    {
        for(int i=3;i>0;--i)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i-1][j]))
            {
                Board[i][j]++;
                Board[i-1][j] = 0;
            }
        }
    }
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=3;i>=0;--i)
        {
            if(Board[i][j])
            {
                int k = i+1;
                while((k < 4)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k-1][j]);
                    ++k;
                }
            }
        }
    }
    DisplayBoard();
    qApp->processEvents();
    AddTile(Board,Depth);
    DisplayBoard();
}

void Game_Window::UpdateBoardUp()
{
    if(Debug)
        qDebug() << "In UpdateBoardUp";
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=0;i<4;++i)
        {
            if(Board[i][j])
            {
                int k = i-1;
                while((k >=0)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k+1][j]);
                    --k;
                }
            }
        }
    }
    for(int j=0;j<4;++j)        //Merge
    {
        for(int i=0;i<3;++i)
        {
            if((Board[i][j]) && (Board[i][j] == Board[i+1][j]))
            {
                Board[i][j]++;
                Board[i+1][j] = 0;
            }
        }
    }
    for(int j=0;j<4;++j)        //Swap
    {
        for(int i=0;i<4;++i)
        {
            if(Board[i][j])
            {
                int k = i-1;
                while((k >=0)  && (Board[k][j] == 0))
                {
                    std::swap(Board[k][j],Board[k+1][j]);
                    --k;
                }
            }
        }
    }
    DisplayBoard();
    qApp->processEvents();
    AddTile(Board,Depth);
    DisplayBoard();
}

void Game_Window::InitialBoard()
{
    int p1 = rand()%16;
    int p2 = rand()%16;
    int p3 = rand()%16;
    int p4 = rand()%16;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            Board[i][j] = 0;
    Board[p1/4][p1%4] = (((rand()%2) && !(rand()%2)) && (rand()%2))+1;
    Board[p2/4][p2%4] = (((rand()%2) && !(rand()%2)) && (rand()%2))+1;
    Board[p3/4][p3%4] = (((rand()%2) && !(rand()%2)) && (rand()%2))+1;
    Board[p4/4][p4%4] = (((rand()%2) && !(rand()%2)) && (rand()%2))+1;
    DisplayBoard();
}

void Game_Window::DisplayBoard()
{
    for(int i=0;i<4;++i)
    {
        for(int j=0;j<4;++j)
        {
            if(Board[i][j])
                ui->GameBoard->item(i,j)->setData(0,1<<Board[i][j]);
            else
                ui->GameBoard->item(i,j)->setData(0,"");
        }
    }
    qDebug();
    for(int i=0;i<4;++i)
        qDebug("%6d%6d%6d%6d",(Board[i][0]?1<<Board[i][0]:0),(Board[i][1]?1<<Board[i][1]:0),(Board[i][2]?1<<Board[i][2]:0),(Board[i][3]?1<<Board[i][3]:0));
    qDebug();
}

void Game_Window::GameOver()
{
    Left_key->disconnect();
    Right_key->disconnect();
    Up_key->disconnect();
    Down_key->disconnect();
    ui->textBrowser->show();
    ui->textBrowser->setText("  Game Over!");
}

Game_Window::~Game_Window()
{
    free(Board);
    delete ui;
}

void Game_Window::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

Thread::Thread()
{
}

void Thread::run()
{
    this->quit();
}

