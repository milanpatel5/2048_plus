# README #

### 2048 Game ###

This game is Developed on Qt platform. We have implemented minimax algorithm with a proper heuristic function to predict position of new tile on the board.

Have a look at [presentation](https://drive.google.com/open?id=1J_wM1zgdv0XyKNwd5S9QBifjoaw3TJOfd3ljiQY-Eok) of this project!