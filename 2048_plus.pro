#-------------------------------------------------
#
# Project created by QtCreator 2015-11-05T10:52:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048_plus
TEMPLATE = app


SOURCES += main.cpp\
        game_window.cpp \

HEADERS  += game_window.h \

FORMS    += game_window.ui

RESOURCES += \
    Resources.qrc
