#ifndef THREAD_H
#define THREAD_H

#include <QThread>

class Thread : public QThread
{
    public:
        Thread();
    
    signals:
    
    public slots:
        void run();
        void AddTile(int x,int y);
    
};

#endif // THREAD_H
